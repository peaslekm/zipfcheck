/*Takes in a large text file (book) and prints out  histogram of
 * letter frequency, uses/letters
 * This as mainly written to practice using maps an iterators
 */

#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "unistd.h"
#include <vector>
#include <map>
#include <cmath>

using namespace std;

unsigned int getWordCount(string text) {
  unsigned int word_count = 0;
  // Adds a space to stop combinations of this word from appearing.
  // finds words from spaces, compares the found word to the word given.
  for (unsigned int i = 0; i < text.length(); i++) {
    if (text[i] == ' ') {
      word_count++;
    }
  }
  return word_count;
}

vector<int> getLettercount(string text, int &charater_count){
		int count = 0;
		vector<int> alphabet;
	for(int i = 97; i < 97+26; i++){
		for(int j = 0; j < text.size(); j++){
			if(i == text[j] || i-32 == text[j]){
				charater_count++;
				count++;
			}
		}
		if(count == 0){
			alphabet.push_back(count);
	}
	else{
		alphabet.push_back(count);
		count = 0;
}

}
	return alphabet;
}



int main(int argc, char **argv){

	fstream file;


	string text = " ";
	string line = " ";
	multimap<int,char> letters2;
	vector<int> letters;
	int charaters_count = 0;
	ifstream out(argv[1]);
	string filename = argv[1];
	//file.open(filename.c_str());
  file.open(filename.c_str(), ios::in);
 	if(file.is_open()){
  	while(file){
      getline(file, line);
      text = text + line + "\n";
	}
}
else{
	cout << "File does not exist or not valid";
	return 0;

}
	file.close();
	letters = getLettercount(text, charaters_count);

		/*Creates a multimap that stores the value in the key
		 *and the letter in the value so I don't have to worry about
		 *sorting the letters with the values
		 */

	for(int i = 0; i < letters.size(); i++){
			letters2.insert(pair<int,char>(letters[i], (char)i+65));
}

	cout.precision(2); // Limits decimal places


	//Because the multi map is in order, we have to reverse it to get the highest frequency first
	for(std::multimap<int,char>::reverse_iterator it = letters2.rbegin(); it != letters2.rend(); it++){
		cout << it->second << ": " << (double)it->first/charaters_count << "%: ";
		for(int i = 0; i < ceil(((double)it->first/charaters_count)*100); i++){
			cout << "*";
		}
		cout << endl;

	}
}
